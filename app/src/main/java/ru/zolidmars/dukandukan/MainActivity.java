package ru.zolidmars.dukandukan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView textView = (TextView) findViewById(R.id.textView);


        Calendar beginCalendar = getCalendar(2016, Calendar.JANUARY, 20);
        Calendar todayCalendar = nullTime(Calendar.getInstance());
        int ataka = 6;
        int kruiz = 157;
        int zakrp = 225;
        int diet = ataka + kruiz + zakrp;
        long diffAll;
        String text = "";
        Calendar endCalendar = addDays(beginCalendar, diet);
        if (todayCalendar.after(endCalendar) || todayCalendar.equals(endCalendar)) {
            diffAll = diff(todayCalendar, endCalendar);
            if (diffAll > 0) {
                text = "Поздравляем диета окончилась " + String.valueOf(diffAll) + " д. назад!";
            }
            if (diffAll == 0)
                text = "Поздравляем диета кончается сегодня!";
            textView.setText(text);
            return;
        }
        diffAll = diff(endCalendar, todayCalendar) - 2;
        String endText = "\n" + "Осталось " + String.valueOf(diffAll) + " из " + String.valueOf(diet);
        long diffStage;
        Calendar atakaCalendar = addDays(beginCalendar, ataka);
        if (todayCalendar.before(atakaCalendar)) {
            diffStage = diff(todayCalendar, beginCalendar);
            text = "Атака.\nПрошло " + String.valueOf(diffStage)
                    + " из " + String.valueOf(ataka) + endText;
            textView.setText(text);
            return;
        }
        Calendar cruizCalendar = addDays(atakaCalendar, kruiz);
        if (todayCalendar.before(cruizCalendar)) {
            diffStage = diff(todayCalendar, atakaCalendar);
            String tip = (diffStage % 2 == 0) ? "Мясо." : "Овощи.";
            text = "Круиз. " + tip + "\nПрошло " + String.valueOf(diffStage)
                    + " из " + String.valueOf(kruiz) + endText;
            textView.setText(text);
            return;
        }
        Calendar zakrpCalendar = addDays(cruizCalendar, zakrp);
        if (todayCalendar.before(zakrpCalendar)) {
            diffStage = diff(todayCalendar, cruizCalendar);
            text = "Закрепление.\nПрошло " + String.valueOf(diffStage)
                    + " из " + String.valueOf(zakrp) + endText;
            textView.setText(text);
        }

    }

    private static long diff(Calendar calendar, Calendar currentCalendar) {

        calendar = nullTime(calendar);
        currentCalendar = nullTime(currentCalendar);

        // Находим разницу между двумя календарями в милисекундах
        long diff = calendar.getTimeInMillis() - currentCalendar.getTimeInMillis();

        // в секундах
        long seconds = diff / 1000;
        // в минутах
        long minutes = seconds / 60;
        // в часах
        long hours = minutes / 60;
        // в днях
        return (hours / 24) + 1;
    }

    private static Calendar getCalendar(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        return nullTime(calendar);
    }

    private static Calendar nullTime(Calendar calendar) {
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    private static Calendar addDays(Calendar calendar, int days) {
        Calendar newCal = (Calendar) calendar.clone();
        newCal.add(Calendar.DATE, days);
        return newCal;
    }

}
